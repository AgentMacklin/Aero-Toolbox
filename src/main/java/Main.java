import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/* ALSO DON'T TOUCH ANYTHING IN HERE OR THE APP WILL BREAK */
public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("MainWindow.fxml"));
        root.setFocusTraversable(true);
        primaryStage.setTitle("Aerodynamics Toolbox");
        primaryStage.setScene(new Scene(root, 750, 500));
        primaryStage.setMinHeight(500);
        primaryStage.setMinWidth(500);
        primaryStage.show();
    }
}

