package Controllers;

import Equations.Isentropic;
import com.jfoenix.controls.JFXRadioButton;
import com.jfoenix.controls.JFXTextField;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.layout.VBox;

import java.net.URL;
import java.util.ResourceBundle;


public class IsentropicController extends ControllerTemplate {

    private ObservableList<Node> children;
    // Class instance that does all the math and stores all the variables for the isentropic pane.
    private Isentropic eqn_handle = new Isentropic();
    @FXML
    private JFXTextField mach_output;
    @FXML
    private JFXTextField pressure_output;
    @FXML
    private JFXTextField area_output;
    @FXML
    private JFXTextField temp_output;
    @FXML
    private JFXTextField density_output;
    @FXML
    private VBox options_vbox;
    private JFXRadioButton sonic_radio = new JFXRadioButton("SUPERSONIC");

    public void initialize(URL location, ResourceBundle resources) {

        this.elements = FXCollections.observableArrayList(
                "P" + "/" + P0,              // Pressure
                "T" + "/" + T0,
                RHO + "/" + RHO0,
                "A/A*",
                "MACH"
        );

        this.inverted_elements = FXCollections.observableArrayList(
                P0 + "/" + "P",              // Pressure
                T0 + "/" + "T",
                RHO0 + "/" + RHO,
                "A*/A",
                "MACH"
        );
        input_combo.getItems().removeAll(input_combo.getItems());
        input_combo.getItems().addAll(elements);
        input_combo.setValue(elements.get(0));
        input_field.setPromptText(get_combo_element().toUpperCase());
        outputs = new JFXTextField[]{pressure_output, temp_output, density_output, area_output, mach_output};
        this.children = this.options_vbox.getChildren();
        initialize_prompts();
    }

    private void is_supersonic_selected(String input, String gamma, String userSelection, ObservableList<String>
            options) {
        if (this.children.contains(this.sonic_radio) && sonic_radio.isSelected()) {
            this.eqn_handle.supersonic = true;
            this.eqn_handle.get_input(input, gamma, userSelection, inverted_elements);
        } else if (this.children.contains(this.sonic_radio) && !sonic_radio.isSelected()) {
            this.eqn_handle.supersonic = false;
            this.eqn_handle.get_input(input, gamma, userSelection, inverted_elements);
        } else {
            this.eqn_handle.get_input(input, gamma, userSelection, inverted_elements);
        }

    }

    // Update the eqn_handle.
    void set_eqn_handle(String input, String gamma, String userSelection) {
        if (invert_elements.isSelected()) {
            is_supersonic_selected(input, gamma, userSelection, inverted_elements);
        } else if (!invert_elements.isSelected()) {
            is_supersonic_selected(input, gamma, userSelection, elements);
        }
    }

    private void change_vbox(Boolean remove) {
        if (remove) {
            options_vbox.getChildren().remove(this.sonic_radio);
        } else if (!remove) {
            options_vbox.getChildren().add(0, this.sonic_radio);
        }
    }

    @Override
    void change_prompts() {
        this.input_field.setPromptText(get_combo_element().toUpperCase());
        if (this.input_combo.getValue().equals("A/A*") || this.input_combo.getValue().equals("A*/A")) {
            change_vbox(false);
        } else if (!this.input_combo.getValue().equals("A/A*") || !this.input_combo.getValue().equals("A*/A")) {
            change_vbox(true);
        }
    }

    void display_results() {
        mach_output.setText(to_string(this.eqn_handle.mach));
        pressure_output.setText(to_string(this.eqn_handle.pressure));
        temp_output.setText(to_string(this.eqn_handle.temp));
        density_output.setText(to_string(this.eqn_handle.density));
        area_output.setText(to_string(this.eqn_handle.areaRatio));
    }


}
