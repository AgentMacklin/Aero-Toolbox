package Controllers;

import Equations.Normal;
import com.jfoenix.controls.JFXTextField;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;

import java.net.URL;
import java.util.ResourceBundle;

public class ObliqueController extends ControllerTemplate {


    // Class instance that does all the math and stores all the variables for the normal pane.
    private Normal eqn_handle = new Normal();
    @FXML
    private JFXTextField stag_press_output;
    @FXML
    private JFXTextField mach_output;
    @FXML
    private JFXTextField pressure_output;
    @FXML
    private JFXTextField press_stag_press_output;
    @FXML
    private JFXTextField temp_output;
    @FXML
    private JFXTextField density_output;

    public void initialize(URL location, ResourceBundle resources) {

        // Combo-box stuff.
        this.elements = FXCollections.observableArrayList(
                "M" + SUB1,
                P2 + "/" + P1,              // Pressure
                T2 + "/" + T1,
                RHO2 + "/" + RHO1,
                P2 + "/" + P0 + SUB1,
                P0 + SUB2 + "/" + P0 + SUB1
        );

        this.inverted_elements = FXCollections.observableArrayList(
                "M" + SUB2,
                P1 + "/" + P2,              // Pressure
                T1 + "/" + T2,
                RHO1 + "/" + RHO2,
                P1 + "/" + P0 + SUB2,
                P0 + SUB1 + "/" + P0 + SUB2
        );

        input_combo.getItems().removeAll(input_combo.getItems());
        input_combo.getItems().addAll(elements);
        input_combo.setValue(elements.get(0));
        input_field.setPromptText(get_combo_element());
        outputs = new JFXTextField[]{mach_output, pressure_output, temp_output,
                density_output, stag_press_output, press_stag_press_output};
        initialize_prompts();
    }

    private void set_mach_prompt() {
        if (!invert_elements.isSelected()) {
            mach_output.setPromptText(inverted_elements.get(0));
        }
        if (invert_elements.isSelected()) {
            mach_output.setPromptText(elements.get(0));
        }
    }

    private void set_prompts(JFXTextField field, int index) {
        if (!invert_elements.isSelected()) {
            field.setPromptText(this.elements.get(index));
        }
        if (invert_elements.isSelected()) {
            field.setPromptText(this.inverted_elements.get(index));
        }
    }

    @Override
    void change_prompts() {
        for (int i = 0; i < outputs.length; i++) {
            if (outputs[i].equals(mach_output)) {
                set_mach_prompt();
            } else {
                set_prompts(outputs[i], i);
            }
        }
    }


    // Update the eqn_handle.
    void set_eqn_handle(String input, String gamma, String userSelection) {
        if (invert_elements.isSelected()) {
            this.eqn_handle.get_input(input, gamma, userSelection, inverted_elements);
        } else if (!invert_elements.isSelected()) {
            this.eqn_handle.get_input(input, gamma, userSelection, elements);
        }
    }


    void display_results() {
        mach_output.setText(to_string(this.eqn_handle.machTwo));
        pressure_output.setText(to_string(this.eqn_handle.pressure));
        temp_output.setText(to_string(this.eqn_handle.temp));
        density_output.setText(to_string(this.eqn_handle.density));
        stag_press_output.setText(to_string(this.eqn_handle.one_stag_press));
        press_stag_press_output.setText(to_string(this.eqn_handle.two_stag_press));
    }


}
