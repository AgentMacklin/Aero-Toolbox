package Controllers;

import com.jfoenix.controls.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

import java.net.URL;
import java.util.ResourceBundle;


abstract class ControllerTemplate implements Initializable {

    static final String SUB1 = "\u2081";
    static final String SUB2 = "\u2082";
    static final String RHO = "\u03c1";
    static final String P1 = "P" + SUB1;
    static final String P2 = "P" + SUB2;
    static final String T1 = "T" + SUB1;
    static final String T2 = "T" + SUB2;
    static final String RHO1 = RHO + SUB1;
    static final String RHO2 = RHO + SUB2;
    private static final String SUB0 = "\u2080";
    static final String RHO0 = RHO + SUB0;
    static final String P0 = "P" + SUB0;
    static final String T0 = "T" + SUB0;

    @FXML
    protected JFXTextField gamma_field;
    @FXML
    protected JFXButton enter_button;
    @FXML
    protected JFXComboBox<String> input_combo;
    @FXML
    protected JFXTextField input_field;
    @FXML
    protected JFXSlider sigfig_slider;
    @FXML
    protected JFXRadioButton invert_elements;

    // Need this, text fields are added to it in initialize so they can be iterated through.
    JFXTextField[] outputs;

    // Combo-box stuff.
    ObservableList<String> elements = FXCollections.observableArrayList();
    ObservableList<String> inverted_elements = FXCollections.observableArrayList();

    // Update the eqn_handle.
    abstract void set_eqn_handle(String input, String gamma, String userSelection);

    // Initialize is like a constructor for the templates
    public abstract void initialize(URL location, ResourceBundle resources);

    // self explanatory
    abstract void display_results();


    @FXML
    void enter_pressed(KeyEvent ke) {
        if (ke.getCode() == KeyCode.ENTER) {
            enter();
        }
    }

    @FXML
    void change_prompts() {
        input_field.setPromptText(get_combo_element());
    }

    @FXML
    void invert_all_elements() {
        change_combobox();
        invert_elements();
        invert_results();
    }

    // Gather all data and crunch the numbers.
    @FXML
    private void enter() {
        String userSelection = get_combo_element();
        String input = input_field.getText();
        String gamma = gamma_field.getText();
        set_eqn_handle(input, gamma, userSelection);
        display_results();
    }

    // Can't call display_results directly from the slider because it crashes the program for some reason.
    @FXML
    void change_rounding() {
        try {
            display_results();
        } catch (NullPointerException e) {
            System.out.println(e.getMessage());
        }
    }


    void initialize_prompts() {
        for (int i = 0; i < outputs.length; i++) {
            outputs[i].setPromptText(this.elements.get(i));
        }
    }

    // Rounding and whatnot.
    String to_string(double num) {
        int rounding = (int) sigfig_slider.getValue();
        String round = Integer.toString(rounding);
        return String.format("%." + round + "f", num);
    }

    private void invert_elements() {
        try {
            if (invert_elements.isSelected()) {
                for (int i = 0; i < outputs.length; i++) {
                    outputs[i].setPromptText(inverted_elements.get(i));
                }
            } else if (!invert_elements.isSelected()) {
                for (int i = 0; i < outputs.length; i++) {
                    outputs[i].setPromptText(elements.get(i));
                }
            }
        } catch (NullPointerException | IndexOutOfBoundsException e) {
            System.out.println(e.getMessage());
        }
    }


    String get_combo_element() {
        try {
            return input_combo.getValue();
        } catch (NullPointerException e) {
            input_combo.setValue(elements.get(0));
            return elements.get(0); // Return PRESSURE
        }
    }

    private void change_combobox() {
        if (invert_elements.isSelected()) {
            String currentSelection = input_combo.getValue();
            int index = elements.indexOf(currentSelection);
            try {
                input_combo.setValue(inverted_elements.get(index));
            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.println(e.getMessage());
            }
        } else if (!invert_elements.isSelected()) {
            String currentSelection = input_combo.getValue();
            int index = inverted_elements.indexOf(currentSelection);
            try {
                input_combo.setValue(elements.get(index));
            } catch (ArrayIndexOutOfBoundsException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    private void invert_results() {
        try {
            for (JFXTextField output : outputs) {
                if (!output.getId().equals("mach_output")) {
                    double current = Double.parseDouble(output.getText());
                    output.setText(to_string(Math.pow(current, -1)));
                }
            }
        } catch (NumberFormatException e) {
            System.out.println(e.getMessage());
        } catch (NullPointerException e) {
            System.out.println(e.getMessage());
        }
    }

}
