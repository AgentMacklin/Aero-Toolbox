package Equations;

import javafx.collections.ObservableList;

public class Isentropic {

    public double pressure;
    public double temp;
    public double density;
    public double areaRatio;
    public double mach;
    // Choose whether to display subsonic area-mach values or supersonic values
    public Boolean supersonic = false;
    private double inputRatio;
    private double gamma;
    private Pressure pressObj = new Pressure();
    private Temperature tempObj = new Temperature();
    private Density densityObj = new Density();
    private Mach machObj = new Mach();
    private AreaRatio areaObj = new AreaRatio();


    public void get_input(String inputRatio, String gamma, String userInput, ObservableList<String> options) {

        this.inputRatio = toDouble(inputRatio);
        this.gamma = toDouble(gamma);

        if (userInput.equals(options.get(0))) {
            pressObj.update();
        } else if (userInput.equals(options.get(1))) {
            tempObj.update();
        } else if (userInput.equals(options.get(2))) {
            densityObj.update();
        } else if (userInput.equals(options.get(3))) {
            areaObj.update();
        } else if (userInput.equals(options.get(4))) {
            machObj.update();
        }
    }


    private double toDouble(String input) {
        try {
            return Double.parseDouble(input);
        } catch (NumberFormatException e) {
            System.err.println("Can't convert that to a double.");
            return 0;
        }
    }

    abstract class Equation {
        /* TEMPLATE FOR THE EQUATIONS BELOW */
        double pressure;
        double temp;
        double density;
        double mach;
        double areaRatio;

        Equation() {
        }

        void get_input() {
            Isentropic.this.inputRatio = inputRatio;
            Isentropic.this.gamma = gamma;
        }

        void update() {
            get_input();
            setMach();
            setPressure();
            setTemp();
            setDensity();
            setAreaRatio();
            Isentropic.this.pressure = this.pressure;
            Isentropic.this.temp = this.temp;
            Isentropic.this.density = this.density;
            Isentropic.this.mach = this.mach;
            Isentropic.this.areaRatio = this.areaRatio;
        }

        abstract void setPressure();

        abstract void setTemp();

        abstract void setDensity();

        abstract void setMach();

        void setAreaRatio() {
            double a = Math.pow((2 / (gamma + 1)) * (1 + ((gamma - 1) / 2) * (mach * mach)), ((gamma + 1) / (2 * (gamma - 1))));
            this.areaRatio = a / mach;
        }

    }

    private class Density extends Equation {

        /* CLASS NAME STANDS FOR INPUT RATIO */

        void setPressure() {
            double power = gamma;
            this.pressure = Math.pow(inputRatio, power);
        }

        void setTemp() {
            double power = gamma - 1;
            this.temp = Math.pow(inputRatio, power);
        }

        void setMach() {
            double a = Math.pow(inputRatio, -(gamma - 1));
            double b = (2 / (gamma - 1)) * (a - 1);
            this.mach = Math.sqrt(b);
        }

        void setDensity() {
            this.density = inputRatio;
        }
    }

    private class Mach extends Equation {

        private Mach() {
        }

        void setMach() {
            this.mach = inputRatio;
        }

        void setPressure() {
            double power = -gamma / (gamma - 1);
            double a = 1 + ((gamma - 1) / 2) * (mach * mach);
            this.pressure = Math.pow(a, power);
        }

        void setDensity() {
            double power = -1 / (gamma - 1);
            double a = 1 + ((gamma - 1) / 2) * (mach * mach);
            this.density = Math.pow(a, power);
        }

        void setTemp() {
            double a = 1 + ((gamma - 1) / 2) * (mach * mach);
            this.temp = Math.pow(a, -1);
        }

    }

    private class Temperature extends Equation {


        void setTemp() {
            this.temp = inputRatio;
        }

        void setPressure() {
            double power = gamma / (gamma - 1);
            this.pressure = Math.pow(inputRatio, power);
        }

        void setDensity() {
            double power = gamma / (gamma * (gamma - 1));
            this.density = Math.pow(inputRatio, power);
        }

        void setMach() {
            double a = Math.pow(inputRatio, -1);
            double b = (2 / (gamma - 1)) * (a - 1);
            this.mach = Math.sqrt(b);
        }

    }

    private class Pressure extends Equation {

        void setPressure() {
            this.pressure = inputRatio;
        }

        void setTemp() {
            double power = (gamma - 1) / gamma;
            this.temp = Math.pow(inputRatio, power);
        }

        void setDensity() {
            double power = 1 / gamma;
            this.density = Math.pow(inputRatio, power);
        }

        void setMach() {
            double a = Math.pow(inputRatio, (-(gamma - 1) / gamma));
            double b = (2 / (gamma - 1)) * (a - 1);
            this.mach = Math.sqrt(b);
        }

    }

    private class AreaRatio extends Mach {

        NumericalSolver solutions = new NumericalSolver();

        @Override
        void setAreaRatio() {
            this.areaRatio = inputRatio;
        }

        @Override
        void setMach() {
            if (supersonic) {
                this.mach = solutions.supersonicAreaRatio(inputRatio, gamma);
            } else if (!supersonic) {
                this.mach = solutions.subsonicAreaRatio(inputRatio, gamma);
            }
        }

    }
}
