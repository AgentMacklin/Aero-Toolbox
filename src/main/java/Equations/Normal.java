package Equations;


import javafx.collections.ObservableList;

public class Normal {

    public double machOne;
    public double machTwo;
    public double pressure;
    public double temp;
    public double density;
    public double one_stag_press;
    public double two_stag_press;
    private double inputRatio;
    private double gamma;

    // Objects and whatnot.
    private Pressure pressObj = new Pressure();
    private Temperature tempObj = new Temperature();
    private Density densityObj = new Density();
    private Mach machObj = new Mach();
    private OneStagnationPressure oneStagObj = new OneStagnationPressure();
    private TwoStagnationPressure twoStagObj = new TwoStagnationPressure();


    public void get_input(String inputRatio, String gamma, String userInput, ObservableList<String> selection) {

        this.inputRatio = toDouble(inputRatio);
        this.gamma = toDouble(gamma);

        // Tried using switch but it complained about selection not being constant. So we're stuck using if statements.
        if (userInput.equals(selection.get(0))) {
            machObj.update();
        } else if (userInput.equals(selection.get(1))) {
            pressObj.update();
        } else if (userInput.equals(selection.get(2))) {
            tempObj.update();
        } else if (userInput.equals(selection.get(3))) {
            densityObj.update();
        } else if (userInput.equals(selection.get(4))) {
            oneStagObj.update();
        } else if (userInput.equals(selection.get(5))) {
            twoStagObj.update();
        }

    }

    private double toDouble(String input) {
        try {
            return Double.parseDouble(input);
        } catch (NumberFormatException e) {
            System.err.println("Can't convert that to a double.");
            return 0;
        }
    }

    /* CLASS NAME STANDS FOR INPUT RATIO */

    abstract class Equation {
        /* TEMPLATE FOR THE EQUATIONS BELOW */
        double machOne;
        double machTwo;
        double pressure;
        double temp;
        double density;
        double one_stag_press;
        double two_stag_press;

        void get_input() {
            Normal.this.inputRatio = inputRatio;
            Normal.this.gamma = gamma;
        }

        void update() {
            get_input();
            setMachOne();
            setMachTwo();
            setPressure();
            setTemp();
            setDensity();
            setOnePress();
            setTwoPress();
            Normal.this.machOne = this.machOne;
            Normal.this.machTwo = this.machTwo;
            Normal.this.pressure = this.pressure;
            Normal.this.temp = this.temp;
            Normal.this.density = this.density;
            Normal.this.one_stag_press = this.one_stag_press;
            Normal.this.two_stag_press = this.two_stag_press;
        }

        abstract void setMachOne();

        void setMachTwo() {
            // Remember input is the machOne before the shock.
            double a = ((gamma - 1) * Math.pow(machOne, 2)) + 2;
            double b = ((2 * gamma) * Math.pow(machOne, 2)) - (gamma - 1);
            this.machTwo = Math.sqrt(a / b);
        }

        void setPressure() {
            this.pressure = (2 * gamma * (machOne * machOne) - (gamma - 1)) / (gamma + 1);
        }

        void setTemp() {
            double a = (2 * gamma * (machOne * machOne) - (gamma - 1));
            double b = ((gamma - 1) * (machOne * machOne) + 2);
            double c = Math.pow((gamma + 1), 2) * (machOne * machOne);
            this.temp = (a * b) / c;
        }

        void setDensity() {
            double a = (gamma + 1) * (machOne * machOne);
            double b = (gamma - 1) * (machOne * machOne) + 2;
            this.density = a / b;
        }

        // Thanks Anderson
        void setOnePress() {
            double aTop = Math.pow(gamma + 1, 2) * Math.pow(machOne, 2);
            double aBot = (4 * gamma) * Math.pow(machOne, 2) - 2 * (gamma - 1);
            double a = Math.pow(aTop / aBot, gamma / (gamma - 1));
            double bTop = 1 - gamma + (2 * gamma * Math.pow(machOne, 2));
            double bBot = gamma + 1;
            this.one_stag_press = a * (bTop / bBot);
        }

        void setTwoPress() {
            double a = ((gamma + 1) * (machOne * machOne)) / (((gamma + 1) * (machOne * machOne)) + 2);
            double b = (gamma + 1) / (2 * gamma * (machOne * machOne) - (gamma - 1));
            this.two_stag_press = Math.pow(a, (gamma / (gamma - 1))) * Math.pow(b, 1 / (gamma - 1));
        }

    }

    private class Mach extends Equation {
        void setMachOne() {
            this.machOne = inputRatio;
        }
    }

    private class Pressure extends Equation {

        @Override
        void setPressure() {
            this.pressure = inputRatio;
        }

        @Override
        void setMachOne() {
            double a = (inputRatio * (gamma + 1) - (gamma - 1) * (1 / (2 * gamma)));
            this.machOne = Math.sqrt(a);
        }
    }

    private class Temperature extends Equation {

        @Override
        void setTemp() {
            this.temp = inputRatio;
        }

        @Override
        void setMachOne() {
            double a = Math.pow(inputRatio, -1);
            double b = (2 / (gamma - 1)) * (a - 1);
            this.machOne = Math.sqrt(b);
        }
    }

    private class Density extends Equation {

        @Override
        void setPressure() {
            this.pressure = inputRatio;
        }

        @Override
        void setMachOne() {
            double a = ((gamma + 1) * Math.pow(inputRatio, -1)) - (gamma - 1);
            this.machOne = Math.sqrt(2 / a);
        }

    }

    private class OneStagnationPressure extends Equation {

        @Override
        void setOnePress() {
            this.one_stag_press = inputRatio;
        }

        @Override
        void setMachOne() {

        }

    }

    private class TwoStagnationPressure extends Equation {

        @Override
        void setTwoPress() {
            this.two_stag_press = inputRatio;
        }

        @Override
        void setMachOne() {

        }


    }
}
