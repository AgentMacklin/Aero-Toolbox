package Equations;


class NumericalSolver {

    private double tolerance = 0.0000000001;

    // Based on Newton-Raphson but higher order
    // Can't believe this one actually works.
    private double iterateSubAreaRatio(double P, double X, double Q, double R) {

        double f, fp, fpp;

        do {
            f = Math.pow(P + (Q * X), 1 / Q) - (R * X);
            fp = Math.pow(P + (Q * X), (1 / Q) - 1) - R;
            fpp = (-(Q - 1) * Math.pow(P + (Q * X), (1 / Q) - 2));

            X = X - (2 * f / (fp - Math.sqrt(Math.pow(fp, 2) - (2 * f * fpp))));

        } while (Math.abs(f) > tolerance);

        return X;
    }

    // Supersonic area ratio
    // Can't believe this works now too.
    private double iterateSuperAreaRatio(double P, double X, double Q, double R) {

        double f, fp, fpp;

        do {

            f = Math.pow((P * X) + Q, (1 / P)) - (R * X);
            fp = Math.pow((P * X) + Q, (1 / P) - 1) - R;
            fpp = (-(P - 1) * Math.pow((P * X) + Q, (1 / P) - 2));

            X = X - ((2 * f) / (fp - Math.sqrt(Math.pow(fp, 2) - (2 * f * fpp))));

        } while (Math.abs(f) > tolerance);

        return X;
    }

    // Can't solve for mach directly so it has to be solved numerically.
    // Link to solution: https://www.grc.nasa.gov/www/winddocs/utilities/b4wind_guide/mach.html
    public double subsonicAreaRatio(double input, double gam) {
        double P = 2 / (gam + 1);
        double Q = 1 - P;
        double R = Math.pow(input, 2);  // A/A*
        double a = Math.pow(P, (1 / Q));
        double r = (R - 1) / (2 * a);
        double X = 1 / ((1 + r) + Math.sqrt(r * (r + 2)));
        double out = iterateSubAreaRatio(P, X, Q, R);
        return Math.sqrt(out);
    }

    public double supersonicAreaRatio(double input, double gam) {
        double P = 2 / (gam + 1);
        double Q = 1 - P;
        double R = Math.pow(input, ((2 * Q) / P));  // A/A*
        double a = Math.pow(Q, (1 / P));
        double r = (R - 1) / (2 * a);
        double X = 1 / ((1 + r) + Math.sqrt(r * (r + 2)));
        double out = iterateSuperAreaRatio(P, X, Q, R);
        return 1 / Math.sqrt(out);
    }

}
