import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.Initializable;
import org.junit.Test;

import java.net.URL;
import java.util.ResourceBundle;

class IsentropicTest implements Initializable {

    private static final String SUB1 = "\u2081";
    private static final String SUB2 = "\u2082";
    private static final String RHO = "\u03c1";
    static final String P1 = "P" + SUB1;
    static final String P2 = "P" + SUB2;
    static final String T1 = "T" + SUB1;
    static final String T2 = "T" + SUB2;
    static final String RHO1 = RHO + SUB1;
    static final String RHO2 = RHO + SUB2;
    private static final String SUB0 = "\u2080";
    private static final String RHO0 = RHO + SUB0;
    private static final String P0 = "P" + SUB0;
    private static final String T0 = "T" + SUB0;
    private String input = "2";
    private String gamma = "1.4";
    private String selection = "MACH";

    public void initialize(URL location, ResourceBundle resources) {
        ObservableList<String> elements = FXCollections.observableArrayList(
                "P" + "/" + P0,              // Pressure
                "T" + "/" + T0,
                RHO + "/" + RHO0,
                "A/A*",
                "MACH"
        );
    }

    @Test
    public void get_input() {
    }

}